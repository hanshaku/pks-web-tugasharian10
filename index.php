<?php require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");

echo "Nama :  " . $sheep->name . "<br>"; // "shaun"
echo "Kaki :  " . $sheep->legs . "<br>"; // 4
echo "Darah dingin : " . $sheep->cold_blooded . "<br>"; // "no"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama :  " . $sungokong->name . "<br>";
echo "Kaki :  " . $sungokong->legs . "<br>";
echo "Darah dingin : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell() . "<br><br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Nama :  " . $kodok->name . "<br>";
echo "Kaki :  " . $kodok->legs . "<br>";
echo "Darah dingin : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump(); // "hop hop"
